FROM nginx:latest
USER root
RUN apt-get update -y
COPY nginx.conf /etc/nginx/nginx.conf
COPY index.html /usr/share/nginx/html/index.html
CMD ["nginx"]